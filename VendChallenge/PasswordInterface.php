<?php

namespace VendChallenge;

/**
 * PasswordInterface
 * 
 * I added an interface so it would be somewhat simple to replace the whole
 * implementation if wanted.
 *
 * @author oliver
 */
interface PasswordInterface
{
    /**
     * constructor requires a storage object and a userid
     * 
     * @param \VendChallenge\StorageInterface $storage
     * @param integer $userId
     */
    public function __construct(StorageInterface $storage, $userId);
    
    /**
     * saves the given password
     * 
     * @param string $password
     */
    public function savePassword($password);
    
    /**
     * validates the given password (against the stored one)
     * 
     * @param string $password
     */
    public function isPasswordValid($password);
    
    /**
     * sets a hash algorithm
     * 
     * @param string $hash
     */
    public function setHash($hash);
    
    /**
     * sets a hash algorithm that the class will try to migrate the stored
     * password to (when validating).
     * 
     * @param string $hash
     */
    public function setMigrateHash($hash);
}
