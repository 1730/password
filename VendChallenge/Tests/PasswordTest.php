<?php

namespace VendChallenge\Tests;

/**
 * The PasswordTest which tests the Password class.
 * It also tests some of the algorithms, ideally these tests would get
 * refactored out into their own test classes!
 */
class PasswordTest extends \PHPUnit_Framework_TestCase
{
    /**
     * helper function which returns a very basic storage mock object.
     * 
     * @return object
     */
    private function getStorageMock()
    {
        $storage = $this->getMockBuilder('VendChallenge\StorageInterface')
          ->setMethods(array('set', 'get'))
          ->getMock();

        return $storage;
    }

    /**
     * saves a password with SHA1
     */
    public function testSaveSha1Password()
    {
        $storage = $this->getStorageMock();

        $storage->expects($this->once())
          ->method('set')
          ->with(
            $this->equalTo(1), $this->equalTo(
              'sha1'
              . \VendChallenge\Password::DIVIDER
              . 'dc724af18fbdd4e59189f5fe768a5f8311527050'
            )
        );

        $password = new \VendChallenge\Password($storage, 1);
        $password->setHash('sha1');

        $rv = $password->savePassword('testing');

        $this->assertTrue($rv);
    }

    /**
     * creates a SHA1 password, then migrates to bcrypt it whilst verifying it.
     * Can't test the exact bcrypt value as we don't know the salt (could get 
     * hardcoded but I'd rather not).
     */
    public function testMigrateSha1ToBcryptPassword()
    {
        $storage = $this->getStorageMock();

        $storage->expects($this->exactly(2))
          ->method('set')
          ->withConsecutive(
            array(
            $this->equalTo(1),
            $this->equalTo(
              'sha1'
              . \VendChallenge\Password::DIVIDER
              . 'dc724af18fbdd4e59189f5fe768a5f8311527050'
            )
            ), array(
            $this->equalTo(1),
            $this->stringStartsWith(
              'bcrypt'
              . \VendChallenge\Password::DIVIDER
              . '$2y$10$'
            )
            )
        );

        $storage->expects($this->once())
          ->method('get')
          ->will(
            $this->returnValue('sha1--dc724af18fbdd4e59189f5fe768a5f8311527050')
        );

        $password = new \VendChallenge\Password($storage, 1);
        $password->setHash('sha1');

        $rv = $password->savePassword('testing');
        $this->assertTrue($rv);

        $password->setMigrateHash('bcrypt');

        $rv = $password->isPasswordValid('testing');
        $this->assertTrue($rv);
    }

    /**
     * saves a bcrypt password
     */
    public function testSaveBcryptPassword()
    {
        $storage = $this->getStorageMock();

        $storage->expects($this->once())
          ->method('set')
          ->with(
            $this->equalTo(1), $this->stringStartsWith(
              'bcrypt' . \VendChallenge\Password::DIVIDER . '$2y$10$'
            )
        );

        $password = new \VendChallenge\Password($storage, 1);
        $password->setHash('bcrypt');

        $rv = $password->savePassword('testing');

        $this->assertTrue($rv);
    }

    /**
     * validates a SHA1 password
     */
    public function testSha1ValidatePassword()
    {
        $storage = $this->getStorageMock();

        $storage->expects($this->once())
          ->method('get')
          ->will(
            $this->returnValue(
              'sha1'
              . \VendChallenge\Password::DIVIDER
              . 'dc724af18fbdd4e59189f5fe768a5f8311527050'
            )
        );

        $password = new \VendChallenge\Password($storage, 1);

        $rv = $password->isPasswordValid('testing');

        $this->assertTrue($rv);
    }

    /**
     * fails validating an incorrect SHA1 password
     */
    public function testSha1InvalidPassword()
    {
        $storage = $this->getStorageMock();

        $storage->expects($this->once())
          ->method('get')
          ->will(
            $this->returnValue(
              'sha1'
              . \VendChallenge\Password::DIVIDER
              . 'dc724af18fbdc4e59189f5fe768a5f8311527050'
            )
        );

        $password = new \VendChallenge\Password($storage, 1);

        $rv = $password->isPasswordValid('testing');

        $this->assertFalse($rv);
    }

    /**
     * validates a md5 password (!)
     */
    public function testMd5ValidPassword()
    {
        $storage = $this->getStorageMock();

        $storage->expects($this->once())
          ->method('get')
          ->will(
            $this->returnValue(
              'md5'
              . \VendChallenge\Password::DIVIDER
              . 'ae2b1fca515949e5d54fb22b8ed95575'
            )
        );

        $password = new \VendChallenge\Password($storage, 1);

        $rv = $password->isPasswordValid('testing');

        $this->assertTrue($rv);
    }

    /**
     * tries to use a stored password with an unsupported hash algorithm.
     * This would be a quite bad situation as we can't recover directly.
     * 
     * @expectedException VendChallenge\Exception\UnsupportedHashException
     */
    public function testUnsupportedHashValidatePassword()
    {
        $storage = $this->getStorageMock();

        $storage->expects($this->once())
          ->method('get')
          ->will(
            $this->returnValue(
              'invalid' . \VendChallenge\Password::DIVIDER . 'doesntmatter'
            )
        );

        $password = new \VendChallenge\Password($storage, 1);

        $rv = $password->isPasswordValid('testing');
    }

    /**
     * validate a default hashed (bcrypt) password
     */
    public function testDefaultValidatePassword()
    {
        $storage = $this->getStorageMock();

        $storage->expects($this->once())
          ->method('get')
          ->will(
            $this->returnValue(
              'bcrypt'
              . \VendChallenge\Password::DIVIDER
              . '$2y$10$JDJ5JDEwJGRjNzI0YWYxO.8F5ylmw/kW2bndlqPQhmgklbIZ5nPMG'
            )
        );

        $password = new \VendChallenge\Password($storage, 1);

        $rv = $password->isPasswordValid('testing');

        $this->assertTrue($rv);
    }

    /**
     * fails validating an incorrect default hashed password
     */
    public function testDefaultInvalidPassword()
    {
        $storage = $this->getStorageMock();

        $storage->expects($this->once())
          ->method('get')
          ->will(
            $this->returnValue(
              'bcrypt'
              . \VendChallenge\Password::DIVIDER
              . '$2y$10$JDJ5JDEwJddjNzI0YWYxO.8F5ylmw/kW2bndlqPQhmgklbIZ5nPMG'
            )
        );

        $password = new \VendChallenge\Password($storage, 1);

        $rv = $password->isPasswordValid('testing');

        $this->assertFalse($rv);
    }

    /**
     * fails due to password being too short.
     * 
     * @expectedException VendChallenge\Exception\InvalidPasswordException
     */
    public function testFailTooShortSavePassword()
    {
        $storage = $this->getStorageMock();

        $password = new \VendChallenge\Password($storage, 1);

        $rv = $password->savePassword('short');
    }
}
