<?php

namespace VendChallenge\HashAlgorithms;

/**
 * the sha1 algorithm
 * (you wanted this, only slightly better than the md5 one!)
 */
class sha1 implements HashAlgorithmInterface
{
    /**
     * 
     * @param type $password
     * @param type $salt
     * @return string
     */
    public function encrypt($password, $salt = null) {
        $encryptedPassword = \sha1($password);
        
        return $encryptedPassword;
    }
    
    /**
     * 
     * @param type $password
     * @param type $existingPassword
     * @return boolean
     */
    public function isVerified($password, $existingPassword) {
        $encryptedPassword = $this->encrypt($password);
        
        if ($encryptedPassword === $existingPassword) {
            return true;
        }
        
        return false;
    }
}
