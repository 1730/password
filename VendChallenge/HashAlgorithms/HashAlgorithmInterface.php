<?php

namespace VendChallenge\HashAlgorithms;

/**
 * HashAlgorithmInterface
 * 
 * The interface for the HashAlgorithms
 *
 * @author oliver
 */
interface HashAlgorithmInterface
{
    /**
     * encrypt the given password with the given salt if provided.
     * 
     * @param type $password
     * @param type $salt
     * @return string
     */
    public function encrypt($password, $salt = null);
    
    /**
     * verifies the given password against the provided existing password
     * 
     * @param type $password
     * @param type $existingPassword
     * @return boolean
     */
    public function isVerified($password, $existingPassword);
}
