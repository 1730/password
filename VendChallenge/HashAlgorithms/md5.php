<?php

namespace VendChallenge\HashAlgorithms;

/**
 * the md5 algorithm
 * (I gotta be kidding!)
 * 
 */
class md5 implements HashAlgorithmInterface
{
    /**
     * 
     * @param type $password
     * @param type $salt
     * @return string
     */
    public function encrypt($password, $salt = null) {
        $encryptedPassword = \md5($password);
        
        return $encryptedPassword;
    }
    
    /**
     * 
     * @param type $password
     * @param type $existingPassword
     * @return boolean
     */
    public function isVerified($password, $existingPassword) {
        $encryptedPassword = $this->encrypt($password);
        
        if ($encryptedPassword === $existingPassword) {
            return true;
        }
        
        return false;
    }
}
