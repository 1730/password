<?php

namespace VendChallenge\HashAlgorithms;

/**
 * the bcrypt algorithm
 */
class bcrypt implements HashAlgorithmInterface
{
    /**
     * 
     * @param type $password
     * @param type $salt
     * @return string
     */
    public function encrypt($password, $salt = null) {
        $options = array();

        if ($salt) {
            $options['salt'] = $salt;
        }

        $encryptedPassword = \password_hash($password, PASSWORD_BCRYPT, $options);
        
        return $encryptedPassword;
    }
    
    /**
     * 
     * @param type $password
     * @param type $existingPassword
     * @return boolean
     */
    public function isVerified($password, $existingPassword)
    {
        if (\password_verify($password, $existingPassword)) {
            return true;
        }
        
        return false;
    }
}
