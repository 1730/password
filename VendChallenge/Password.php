<?php

namespace VendChallenge;

/**
 * The Password class
 * Also see PasswordInterface
 * 
 * This class can be used to save passwords encrypted with different hashing
 * algorithms. It can be easily extended with new algorithms via simply adding
 * a new HashAlgorithm class (see HashAlgorithmInterface).
 * 
 * This class can also migrate passwords between different hashing algorithms
 * when the password gets verified, on the fly.
 *
 * @author Oliver
 */
class Password implements PasswordInterface
{
    /**
     * divider to be able to store hashing algorithm identifier and encrypted
     * password in the same value.
     */
    const DIVIDER = '--';

    /**
     * The storage object
     *
     * @var object
     */
    private $storage;
    
    /**
     * The userId
     *
     * @var integer
     */
    private $userId;
    
    /**
     * the hashing algorithm (defaults to bcrypt as that's the safest in this 
     * implementation)
     *
     * @var string
     */
    private $hash = 'bcrypt';
    
    /**
     * the hashing algorithm that this class will try to migrate the password to
     *
     * @var string
     */
    private $migrateHash;

    /**
     * constructor requires a storage object and a userid
     * 
     * @param \VendChallenge\StorageInterface $storage
     * @param integer $userId
     */
    public function __construct(StorageInterface $storage, $userId)
    {
        $this->storage = $storage;
        $this->userId = $userId;
    }

    /**
     * saves the given password
     * @TODO add real password validation!
     * 
     * @param string $password
     * @return boolean
     */
    public function savePassword($password)
    {
        if (strlen($password) < 6) {
            throw new Exception\InvalidPasswordException('Password too short!');
        }

        $encryptedPasswordString = $this->generateEncryptedString($password);

        $this->storage->set($this->userId, $encryptedPasswordString);

        return true;
    }

    /**
     * generates the string that gets stored (algo + password)
     * 
     * @param string $password
     * @return string
     */
    private function generateEncryptedString($password)
    {
        $encryptedPassword = $this->encryptPassword($password);
        $encryptedPasswordString = $this->getHashForEncryption()
            . self::DIVIDER
            . $encryptedPassword;

        return $encryptedPasswordString;
    }

    /**
     * validates the given password (against the stored one)
     * Also stores/overwrites a newly encrypted password with the migration
     * algorithm, if set.
     * 
     * @param string $password
     */
    public function isPasswordValid($password)
    {
        $storedPasswordString = $this->storage->get($this->userId);

        if (empty($storedPasswordString)) {
            return false;
        }

        $dividerPosition = strpos($storedPasswordString, self::DIVIDER);
        $hash = substr($storedPasswordString, 0, $dividerPosition);
        $storedPassword = substr(
          $storedPasswordString, ($dividerPosition + strlen(self::DIVIDER))
        );

        $hashAlgorithm = $this->getHashAlgorithm($hash);

        if ($hashAlgorithm->isVerified($password, $storedPassword)) {
            if ($this->getHashForEncryption() !== $hash) {
                try {
                    $this->savePassword($password);
                } catch (Exception $e) {
                    // @TODO log the error
                    // no need for further action, if the save fails the existing 
                    // value will still work and we don't want to interrupt the 
                    // password validation.
                }
            }

            return true;
        }

        return false;
    }

    /**
     * encrypts the given password using the given salt (if any) with the given
     * hash (or default)
     * 
     * @param string $password
     * @param string $salt
     * @param string $hash
     * @return string
     */
    private function encryptPassword($password, $salt = null, $hash = null)
    {
        $encryptedPassword = null;

        if ($hash === null) {
            $hash = $this->getHashForEncryption();
        }

        $algorithm = $this->getHashAlgorithm($hash);

        $encryptedPassword = $algorithm->encrypt($password, $salt);

        return $encryptedPassword;
    }

    /**
     * sets a hash algorithm
     * 
     * @param string $hash
     * @return this
     */
    public function setHash($hash)
    {
        $this->getHashAlgorithm($hash);

        $this->hash = $hash;

        return $this;
    }

    /**
     * sets a hash algorithm that the class will try to migrate the stored
     * password to (when validating).
     * 
     * @param string $hash
     * @return this
     */
    public function setMigrateHash($hash)
    {
        $this->getHashAlgorithm($hash);

        $this->migrateHash = $hash;

        return $this;
    }

    /**
     * returns the hash that should be used for encryption
     * 
     * @return string
     */
    private function getHashForEncryption()
    {
        if ($this->migrateHash) {
            return $this->migrateHash;
        }

        return $this->hash;
    }

    /**
     * gets the hash algorithm object based on the provided hash
     * 
     * @param string $hash
     * @return \VendChallenge\HashAlgorithms\HashAlgorithmInterface object
     * @throws Exception\UnsupportedHashException
     */
    private function getHashAlgorithm($hash)
    {
        $className = '\VendChallenge\HashAlgorithms\\' . $hash;

        if (!class_exists($className)) {
            throw new Exception\UnsupportedHashException(
                'Unsupported hash ' . $hash
            );
        }
        
        $hashAlgorithm = new $className();
        
        if (!$hashAlgorithm instanceof \VendChallenge\HashAlgorithms\HashAlgorithmInterface) {
            throw new Exception\UnsupportedHashException(
                'Unsupported hash ' . $hash
            );
        }
        
        return $hashAlgorithm;
    }

}
