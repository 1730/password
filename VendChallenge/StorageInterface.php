<?php

namespace VendChallenge;

/**
 * StorageInterface
 * 
 * The interface for the super simple key/value store Storage class
 *
 * @author oliver
 */
interface StorageInterface
{
    /**
     * sets the value for the given key
     * 
     * @param string $key
     * @param string $value
     */
    public function set($key, $value);
    
    /**
     * gets the value for the given key
     * 
     * @param type $key
     */
    public function get($key);
}
