<?php

namespace VendChallenge\Exception;

/**
 * UnsupportedHashException
 * 
 * gets thrown when the requested hash algorithm isn't supported / available
 * 
 */
class UnsupportedHashException extends \Exception
{
    
}
