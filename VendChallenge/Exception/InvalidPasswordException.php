<?php

namespace VendChallenge\Exception;

/**
 * InvalidPasswordException
 * 
 * gets thrown when the provided password is invalid for the provided user
 */
class InvalidPasswordException extends \Exception
{
    
}
