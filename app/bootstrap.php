<?php

/**
 * quick autoload implementation for unit tests
 */

if (!function_exists('classAutoLoader')) {
    function classAutoLoader($class)
    {
        $classFile = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';

        if (is_file($classFile) && !class_exists($class)) {
            require_once $classFile;
        }
    }
}

spl_autoload_register('classAutoLoader', true, false);
