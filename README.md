# Password - class

## Overview

This is the Password class challenge implementation. It's actually more than one
class after I refactored it twice, but it makes more sense this way.
It's easily extensible with further algorithms by simply creating new classes.

The easiest way to see it in action is to read and run the unit tests (phpunit).


## Installation

1. Install composer.
2. Run composer install.
3. fix dependencies if necessary and run composer install again.


## Usage

See testing.


## Testing

Run 
```
#!bash
vendor/phpunit/phpunit/phpunit

```
 in the root folder.


## Further thoughts

The HashAlgorithms should have tests of their own, instead these are part of the
PasswordTest suite.

The file / folder structure is quite custom as I didn't build it towards any
existing frameworks.

The "storage" object is extremely simple and would most likely not work in such
a way. I didn't come up with a more generic and less tight binding approach as
I don't think it's important for this exercise.

The hashing algorithms could get extended by passing e.g. additional options via
the constructor (e.g. bcrypt supports different encryption "levels", tradeoff
between performance and security)

I didn't implement any logging, but I would highly recommend doing that.